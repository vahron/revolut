# Money Transfer API
## Revolut Back-End Java Test

### Solution main points
* Event-driven non-blocking
* Clustered event-bus, REST is optional
* Scaling and high-availability out of the box
* Auto-generated proxies and handlers
* Multi-language is supported
* Encapsulated DAO
* Dependency injection
* Unit tests

### Money Transfer overview
#### Technology stack choise
The task is to build money transfer REST API. Desired capabilities include:

* high-availability
* high-performance
* fault-tolerant

To reach the goals I'd use ready-for-production solutions. 

First of all, we want to process transactions without errors,
with constraints and type checking. So it's better to prefer SQL database (used in-memory for now).
One approach for solution is to keep all resources as close to DB as we can. For example, using PL/SQL: 1) allows to deal with transaction issues; 2) gives the best performance; 3) avoids self-coded locks.
But for now, I didn't use PL/SQL.

Then, we don't want to implement infrastructure for clustering and scaling. I don't want to waste time on configuring and orchestrating messaging queues, distributed logging, service-discovery, load-balancing, massive deploying, fault-tolerance.
I found that Vert.x is good to provide all features mentioned above.

Moreover, Vert.x uses event-driven approach, so the code will be more simple in comparison to traditional blocking development.

In Vert.X clients communicate through common event bus. So it's not needed to create REST API, but for the task I exposed Vert.x services to HTTP.

#### Algorithm approach
Solution has users and transactions. User represents bank account with name, balance and currency. It's immutable except balance.

Transaction is money transfer operation. Transaction is done in two steps. First step - create an invoice with TTL. 
Invoice fixes money count and currency exchange rate for operation and also protect from secondary execution.
After invoice processed by engine, money transfer become succeed.

### Technology stack overview
* Java 8
* Gradle
* JUnit
* Vert.x

#### Vert.x
Vert.x - high performance, scalable and fault-tolerant, polyglot solution for building applications. 
Vert.x offers event-driven architecture, micro-services, replication, high-availability, clustering out of the box.

Vert.x uses event-bus to connect all micro-services inside cluster. The project is much useful than plain REST and offers out of the box:

* Auto-generated proxies, handlers, clients for all languages
* Type checking
* Publish/Subscribe, Request-Reply, Point-to-Point messaging instead of only Request-Reply in REST
* Service discovery
* Load balancing
* Cluster shared data
* High-availability in cluster
* Event-driven approach
* On-the-fly builds
* Wide community, production ready

Event-driven approach allows to build application with simple single-thread-like concurrency model (Multi-Reactor pattern). 
Often event-driven approach have bigger throughput with less headache in comparison to traditional multi-thread-blocking code.

Vert.x suggest using following architecture of solution:

* small services (e.g. currency conversion) is connected to event-bus
* clients are connected to event-bus and get messages
* application servers with bunch of small services are also on same bus
* REST proxies (if needed) for services translate every query to event-bus  

It's quite useful when you can develop clustered application without mess, that's why I chose Vert.x as server engine.

#### JSpare Vert.x
As I figured out after building example application, Vert.x has quite primitive API to build REST services and 
event-bus services. I needed a tool that provides handy annotation-driven code generation.
After seeking for a solution, I came out with JSpare Vert.x.
It provides DI, automatic event-bus proxy handling, spring-like REST annotations.

Unfortunately, JSpare Vert.x has no integration with unit tests, 
that's why I had to implement simple test runner from scratch (and that took time).

#### Vert.x susom-database
When it comes to banking applications, it will be all about transactions and databases. By default, Vert.x provides
only JDBC async service without any ORM. I didn't have enough time to implement async-like ORM solution, so I used
Vert.x susom-database as async alternative to plain JDBC. The disadvantage is that I had to code DAO layer, and 
also susom-database doesn't support DB constraints creation.

### Solution
#### DB Scheme
```
#!text
// User record is immutable except balance field
User:
    - id
    - name
    - balance
    - currency

// Transaction has [INVOICE, FAILED, DONE] state and timestamp. Invoice transactions have TTL.
Transaction:
    - id
    - from_account_id
    _ to_account_id
    - requested_count
    - converted_count
    - status: [invoice, failed, done]
    - timestamp (shows created or processed timestamp)
```

#### REST Scheme
All endpoints above should be prepended by version, only v1 is available by now. 
Server starts on localhost:8080 by default.
```
#!text
POST /user/ - create an user
GET /user/ - get all registered users
GET /user/id - get the user by id
POST /user/changeBalance/id - for testing purposes only. Immediately changes balance

POST /transaction/ - create a transaction in invoice status
GET /transaction/ - get all transactions
GET /transaction/id - get transaction info (status, etc)
PUT /transaction/processByInvoice/id - process money transfer by invoice id
```
I placed all REST API classes into single package, because it's usually not needed for Vert.x

#### Examples
##### Create a user
```
#!text
POST /v1/user/?name=test-username&currencyCode=USD -> 200
{
  "id": 1,
  "name": "test-username",
  "balance": 0,
  "currency": "USD"
}
```
##### Change user balance (testing purpose only!)
```
#!text
POST /v1/user/changeBalance/1?change=200 -> 200
{
  "id": 1,
  "name": "test",
  "balance": 200,
  "currency": "USD"
}
```
##### Get user
```
#!text
GET /v1/user/1 -> 200
{
  "id": 1,
  "name": "test-username",
  "balance": 0,
  "currency": "USD"
}
```
##### Create transaction
```
#!text
POST /v1/transaction/?fromAccountId=1&toAccountId=2&requestedCount=2 -> 201
{
  "id": 1,
  "fromAccountId": 1,
  "toAccountId": 2,
  "requestedCount": 2,
  "convertedCount": 4,
  "status": "INVOICE",
  "timestamp": 1492183969329
}
```
##### Process transaction
```
#!text
PUT /v1/transaction/processByInvoice/1 -> 200
{
  "id": 1,
  "fromAccountId": 1,
  "toAccountId": 2,
  "requestedCount": 2,
  "convertedCount": 2,
  "status": "DONE",
  "timestamp": 1492185535314
}
```

### Testing
I implemented simple test runner for unit tests because JSpare has no integration for unit tests. All tests are run over complete server instance.

### Benchmarking
I have Intel Core i7, two physical cores, 8GB RAM.
```
#!text
ab -k -n 20000 -c 1000 -m POST 'http://localhost:8080/v1/user/?name=test&currencyCode=USD'
Requests per second:    21711.90 [#/sec] (mean)
Time per request:       46.058 [ms] (mean)

ab -k -n 20000 -c 1000 -m POST 'http://localhost:8080/v1/transaction/?fromAccountId=1&toAccountId=2&requestedCount=2'
Requests per second:    11978.67 [#/sec] (mean)
Time per request:       83.482 [ms] (mean)


```

**To run, please use provided build-fat.jar**, or compile yourself with 'gradle shadowJar' and result in build/libs/