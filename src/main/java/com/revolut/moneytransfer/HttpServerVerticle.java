package com.revolut.moneytransfer;

import com.revolut.moneytransfer.configuration.ConfigurationService;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jspare.core.annotation.Inject;
import org.jspare.vertx.web.builder.HttpServerBuilder;
import org.jspare.vertx.web.builder.RouterBuilder;

public class HttpServerVerticle extends AbstractVerticle {
    private static final String HTTP_PORT_KEY = "http.port";
    private static final Logger logger = LogManager.getLogger();

    @Inject
    private ConfigurationService config;

    private HttpServer server;

    @Override
    public void start() throws Exception {
        Router router = RouterBuilder.create(vertx).scanClasspath(true).build();

        server = HttpServerBuilder.create(vertx).router(router).build();
        server.listen(config.getConfig().getInteger(HTTP_PORT_KEY), event -> {
            if (event.succeeded()) {
                logger.info("HTTP REST server started at {}", event.result().actualPort());
            } else {
                logger.error("HTTP REST server failed to start", event.cause());
            }
        });
    }

    @Override
    public void stop() throws Exception {
        server.close();
    }
}
