package com.revolut.moneytransfer;

import com.revolut.moneytransfer.configuration.ConfigurationService;
import com.revolut.moneytransfer.configuration.ConfigurationServiceImpl;
import com.revolut.moneytransfer.model.transaction.TransactionDAO;
import com.revolut.moneytransfer.model.user.UserDAO;
import com.revolut.moneytransfer.service.persistence.PersistenceService;
import com.revolut.moneytransfer.service.persistence.PersistenceServiceImpl;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import org.jspare.core.container.ContainerUtils;
import org.jspare.vertx.bootstrap.VerticleInitializer;
import org.jspare.vertx.builder.EventBusBuilder;
import org.jspare.vertx.builder.ProxyServiceBuilder;

import static org.jspare.core.container.Environment.my;
import static org.jspare.core.container.Environment.registryComponent;

public class StartVerticle extends AbstractVerticle {
    public static final String STARTED_CHANNEL = "/started";

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        initializeConfiguration().compose(event -> {
            PersistenceService persistenceService = initializePersistence();

            buildEventBusAndRestServices();

            return CompositeFuture.all(initializeDBSchema(persistenceService), initializeVerticles());
        }).setHandler(event -> {
            if (event.succeeded()) startFuture.complete();
            else startFuture.fail(event.cause());
            vertx.eventBus().send(STARTED_CHANNEL, new JsonObject());
        });
    }

    private Future<JsonObject> initializeConfiguration() {
        ConfigurationService configurationService = new ConfigurationServiceImpl();
        ContainerUtils.processInjection(configurationService);
        registryComponent(configurationService);
        return configurationService.retrieveAndCacheConfig();
    }

    private PersistenceService initializePersistence() {
        PersistenceService persistenceService = new PersistenceServiceImpl();
        ContainerUtils.processInjection(persistenceService);
        registryComponent(persistenceService);
        return persistenceService;
    }

    private void buildEventBusAndRestServices() {
        ProxyServiceBuilder.create(vertx).scanClasspath(true).build();
        EventBusBuilder.create(vertx).scanClasspath(true).build();
    }

    private Future<String> initializeVerticles() {
        Future<String> result = Future.future();
        vertx.deployVerticle(VerticleInitializer.initialize(HttpServerVerticle.class), result);
        return result;
    }

    private Future<Void> initializeDBSchema(PersistenceService persistenceService) {
        Future<Void> result = Future.future();
        persistenceService.transactAsync(dbs -> {
            my(UserDAO.class).createSchema(dbs);
            my(TransactionDAO.class).createSchema(dbs);
            return null;
        }, result);
        return result;
    }
}
