package com.revolut.moneytransfer.configuration;

import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import org.jspare.core.annotation.Component;

@Component
public interface ConfigurationService {
    JsonObject getConfig();

    Future<JsonObject> retrieveAndCacheConfig();
}
