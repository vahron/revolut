package com.revolut.moneytransfer.configuration;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.jspare.vertx.annotation.VertxInject;

public class ConfigurationServiceImpl implements ConfigurationService {
    @VertxInject
    private Vertx vertx;

    private ConfigRetriever retriever;

    @Override
    public JsonObject getConfig() {
        return retriever.getCachedConfig();
    }

    @Override
    public Future<JsonObject> retrieveAndCacheConfig() {
        if (retriever == null) {
            ConfigRetrieverOptions opts = new ConfigRetrieverOptions();

            ConfigStoreOptions store = new ConfigStoreOptions()
                    .setType("json")
                    .setConfig(DefaultConfig.getDefaultConfig());

            opts.addStore(store);

            retriever = ConfigRetriever.create(vertx, opts);
        }
        Future<JsonObject> result = Future.future();
        retriever.getConfig(result);
        return result;
    }
}
