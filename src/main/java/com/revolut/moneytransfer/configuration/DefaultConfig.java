package com.revolut.moneytransfer.configuration;

import io.vertx.core.json.JsonObject;

public class DefaultConfig {
    private final static JsonObject DEFAULT_CONFIG = new JsonObject()
            .put("persistence.database.url", "jdbc:hsqldb:mem:test;shutdown=true")
            .put("transaction.invoice.ttl.ms", 1000000)
            .put("http.port", 8080);

    public static JsonObject getDefaultConfig() {
        return DEFAULT_CONFIG;
    }

    public static void putDefaultConfig(String key, Object value) {
        DEFAULT_CONFIG.put(key, value);
    }
}
