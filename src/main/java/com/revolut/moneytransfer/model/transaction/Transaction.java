package com.revolut.moneytransfer.model.transaction;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

@DataObject(generateConverter = true)
public class Transaction {
    private long id;
    private long fromAccountId;
    private long toAccountId;
    private double requestedCount;
    private double convertedCount;
    private TransactionStatus status;
    private long timestamp;

    public Transaction() {
    }

    public Transaction(JsonObject json) {
        TransactionConverter.fromJson(json, this);
    }

    public JsonObject toJson() {
        JsonObject result = new JsonObject();
        TransactionConverter.toJson(this, result);
        return result;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFromAccountId() {
        return fromAccountId;
    }

    public void setFromAccountId(long fromAccountId) {
        this.fromAccountId = fromAccountId;
    }

    public long getToAccountId() {
        return toAccountId;
    }

    public void setToAccountId(long toAccountId) {
        this.toAccountId = toAccountId;
    }

    public double getRequestedCount() {
        return requestedCount;
    }

    public void setRequestedCount(double requestedCount) {
        this.requestedCount = requestedCount;
    }

    public double getConvertedCount() {
        return convertedCount;
    }

    public void setConvertedCount(double convertedCount) {
        this.convertedCount = convertedCount;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", fromAccountId=" + fromAccountId +
                ", toAccountId=" + toAccountId +
                ", requestedCount=" + requestedCount +
                ", convertedCount=" + convertedCount +
                ", status=" + status +
                ", timestamp=" + timestamp +
                '}';
    }
}
