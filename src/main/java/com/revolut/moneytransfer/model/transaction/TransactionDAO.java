package com.revolut.moneytransfer.model.transaction;

import com.github.susom.database.Database;
import org.jspare.core.annotation.Component;

import java.util.List;
import java.util.function.Supplier;

@Component
public interface TransactionDAO {
    Transaction createTransaction(Supplier<Database> db,
                                  long fromAccountId,
                                  long toAccountId,
                                  double requestedCount,
                                  double convertedCount);

    Transaction getTransaction(Supplier<Database> db, long id);

    Transaction changeTransactionStatus(Supplier<Database> db, long id, TransactionStatus status);

    List<Transaction> getAllTransactions(Supplier<Database> db);

    void createSchema(Supplier<Database> dbs);
}
