package com.revolut.moneytransfer.model.transaction;

import com.github.susom.database.Database;
import com.github.susom.database.Row;
import com.github.susom.database.Schema;
import org.joda.time.DateTimeUtils;

import java.util.List;
import java.util.function.Supplier;

public class TransactionDAOImpl implements TransactionDAO {
    private static final String TRANSACTION_ID_SEQ = "transaction_id_seq";
    private final static String SELECT_ALL_TRANSACTIONS = "select id, from_account_id, to_account_id, requested_count, converted_count, status, timestamp from transaction";
    private final static String SELECT_TRANSACTION_BY_ID = "select id, from_account_id, to_account_id, requested_count, converted_count, status, timestamp from transaction where id=?";
    private final static String INSERT_TRANSACTION = "insert into transaction values(?,?,?,?,?,?,?)";
    private final static String UPDATE_STATUS = "update transaction set status=?, timestamp=? where id=?";

    @Override
    public Transaction createTransaction(Supplier<Database> db,
                                         long fromAccountId,
                                         long toAccountId,
                                         double requestedCount,
                                         double convertedCount) {
        return db.get().toInsert(INSERT_TRANSACTION)
                .argPkSeq(TRANSACTION_ID_SEQ)
                .argLong(fromAccountId)
                .argLong(toAccountId)
                .argDouble(requestedCount)
                .argDouble(convertedCount)
                .argString(TransactionStatus.INVOICE.name())
                .argLong(DateTimeUtils.currentTimeMillis())
                .insertReturning("transaction", "id", rs -> {
                                     rs.next();
                                     return buildTransactionFromRow(rs);
                                 }, "from_account_id", "to_account_id", "requested_count", "converted_count",
                                 "status", "timestamp");
    }

    @Override
    public List<Transaction> getAllTransactions(Supplier<Database> db) {
        return db.get().toSelect(SELECT_ALL_TRANSACTIONS).queryMany(this::buildTransactionFromRow);
    }

    @Override
    public Transaction getTransaction(Supplier<Database> db, long id) {
        return db.get().toSelect(SELECT_TRANSACTION_BY_ID).argLong(id).queryFirstOrThrow(this::buildTransactionFromRow);
    }

    @Override
    public Transaction changeTransactionStatus(Supplier<Database> db, long id, TransactionStatus status) {
        db.get().toUpdate(UPDATE_STATUS)
                .argString(status.name())
                .argLong(DateTimeUtils.currentTimeMillis())
                .argLong(id)
                .update(1);
        return getTransaction(db, id);
    }

    @Override
    public void createSchema(Supplier<Database> dbs) {
        new Schema()
                .addTable("transaction")
                .addColumn("id").primaryKey().table()
                .addColumn("from_account_id").asLong().table()
                .addColumn("to_account_id").asLong().table()
                .addColumn("requested_count").asDouble().table()
                .addColumn("converted_count").asDouble().table()
                .addColumn("status").asString(10).table()
                .addColumn("timestamp").asLong().table()
                .schema().addSequence(TRANSACTION_ID_SEQ).schema().execute(dbs);
    }

    private Transaction buildTransactionFromRow(Row row) {
        Transaction transaction = new Transaction();
        transaction.setId(row.getIntegerOrZero());
        transaction.setFromAccountId(row.getLongOrZero());
        transaction.setToAccountId(row.getLongOrZero());
        transaction.setRequestedCount(row.getDoubleOrZero());
        transaction.setConvertedCount(row.getDoubleOrZero());
        transaction.setStatus(TransactionStatus.valueOf(row.getStringOrEmpty()));
        transaction.setTimestamp(row.getLongOrZero());
        return transaction;
    }
}
