package com.revolut.moneytransfer.model.transaction;

public enum TransactionStatus {
    INVOICE, FAILED, DONE
}
