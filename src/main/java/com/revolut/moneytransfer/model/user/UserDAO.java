package com.revolut.moneytransfer.model.user;

import com.github.susom.database.Database;
import org.jspare.core.annotation.Component;

import java.util.List;
import java.util.function.Supplier;

@Component
public interface UserDAO {
    User createUser(Supplier<Database> db, String name, String currencyCode);

    List<User> fetchAllUsers(Supplier<Database> db);

    User getUser(Supplier<Database> db, long id);

    User changeBalance(Supplier<Database> db, long id, double change);

    void createSchema(Supplier<Database> db);
}

