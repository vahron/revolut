package com.revolut.moneytransfer.model.user;

import com.github.susom.database.Database;
import com.github.susom.database.Row;
import com.github.susom.database.Schema;

import java.util.List;
import java.util.function.Supplier;

public class UserDAOImpl implements UserDAO {
    private static final String USER_ID_SEQ = "user_id_seq";
    private final static String SELECT_ALL_USERS = "select id, name, balance, currency from user";
    private final static String SELECT_USER_BY_ID = "select id, name, balance, currency from user where id=?";
    private final static String INSERT_USER = "insert into user values(?,?,?,?)";
    private final static String UPDATE_BALANCE = "update user set balance=balance + ? where id=?";

    @Override
    public User createUser(Supplier<Database> db, String name, String currencyCode) {
        return db.get().toInsert(UserDAOImpl.INSERT_USER)
                .argPkSeq(USER_ID_SEQ)
                .argString(name)
                .argDouble(0D)
                .argString(currencyCode)
                .insertReturning("user", "id", rs -> {
                    rs.next();
                    return buildUserFromRow(rs);
                }, "name", "balance", "currency");
    }

    @Override
    public List<User> fetchAllUsers(Supplier<Database> db) {
        return db.get().toSelect(UserDAOImpl.SELECT_ALL_USERS).queryMany(this::buildUserFromRow);
    }

    @Override
    public User getUser(Supplier<Database> db, long id) {
        return db.get().toSelect(UserDAOImpl.SELECT_USER_BY_ID).argLong(id).queryFirstOrThrow(this::buildUserFromRow);
    }

    @Override
    public User changeBalance(Supplier<Database> db, long id, double change) {
        db.get().toUpdate(UPDATE_BALANCE).argDouble(change).argLong(id).update(1);
        return getUser(db, id);
    }

    @Override
    public void createSchema(Supplier<Database> db) {
        new Schema()
                .addTable("user")
                .addColumn("id").primaryKey().table()
                .addColumn("name").asString(80).table()
                .addColumn("balance").asDouble().table()
                .addColumn("currency").asString(3).schema()
                .addSequence(USER_ID_SEQ).schema().execute(db);
    }

    private User buildUserFromRow(Row row) {
        User user = new User();
        user.setId(row.getIntegerOrZero());
        user.setName(row.getStringOrEmpty());
        user.setBalance(row.getDoubleOrZero());
        user.setCurrency(row.getStringOrEmpty());
        return user;
    }
}
