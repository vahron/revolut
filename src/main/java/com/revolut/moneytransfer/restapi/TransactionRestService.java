package com.revolut.moneytransfer.restapi;

import com.revolut.moneytransfer.restapi.base.RestApiService;
import com.revolut.moneytransfer.service.transaction.TransactionService;
import org.jspare.vertx.annotation.VertxProxyInject;
import org.jspare.vertx.web.annotation.handler.Handler;
import org.jspare.vertx.web.annotation.handling.Parameter;
import org.jspare.vertx.web.annotation.method.Get;
import org.jspare.vertx.web.annotation.method.Post;
import org.jspare.vertx.web.annotation.method.Put;
import org.jspare.vertx.web.annotation.subrouter.SubRouter;

@SubRouter("/v1/transaction")
public class TransactionRestService extends RestApiService {
    @VertxProxyInject(TransactionService.SERVICE)
    private TransactionService transactionService;

    @Post
    @Handler
    public void createTransaction(@Parameter("fromAccountId") Long fromAccountId,
                                  @Parameter("toAccountId") Long toAccountId,
                                  @Parameter("requestedCount") Double requestedCount) {
        transactionService.createTransaction(fromAccountId, toAccountId, requestedCount, this::handlerCreated);
    }

    @Get
    @Handler
    public void getAllTransactions() {
        transactionService.getAllTransactions(this::handler);
    }

    @Put("/processByInvoice/:id")
    @Handler
    public void processTransaction(@Parameter("id") Long invoiceId) {
        transactionService.processTransaction(invoiceId, this::handler);
    }

    @Get("/:id")
    @Handler
    public void getTransaction(@Parameter("id") Long id) {
        transactionService.getTransaction(id, this::handler);
    }
}
