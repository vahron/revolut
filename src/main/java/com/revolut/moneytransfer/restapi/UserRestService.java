package com.revolut.moneytransfer.restapi;

import com.revolut.moneytransfer.restapi.base.RestApiService;
import com.revolut.moneytransfer.service.user.UserService;
import org.jspare.vertx.annotation.VertxProxyInject;
import org.jspare.vertx.web.annotation.handler.Handler;
import org.jspare.vertx.web.annotation.handling.Parameter;
import org.jspare.vertx.web.annotation.method.Get;
import org.jspare.vertx.web.annotation.method.Post;
import org.jspare.vertx.web.annotation.subrouter.SubRouter;

@SubRouter("/v1/user")
public class UserRestService extends RestApiService {
    @VertxProxyInject(UserService.SERVICE)
    private UserService userService;

    @Post
    @Handler
    public void createUser(@Parameter("name") String name, @Parameter("currencyCode") String currencyCode) {
        userService.createUser(name, currencyCode, this::handlerCreated);
    }

    @Get
    @Handler
    public void fetchAllUsers() {
        userService.fetchAllUsers(this::handler);
    }

    @Get("/:id")
    @Handler
    public void getUser(@Parameter("id") Long id) {
        userService.getUser(id, this::handler);
    }

    @Post("/changeBalance/:id")
    @Handler
    public void changeBalance(@Parameter("id") Long id, @Parameter("change") Double change) {
        userService.changeBalance(id, change, this::handler);
    }
}
