package com.revolut.moneytransfer.restapi.base;

import io.vertx.core.AsyncResult;
import org.jspare.vertx.web.handler.APIHandler;

public class RestApiService extends APIHandler {

    protected <T> void handler(AsyncResult<T> result) {
        if (result.failed()) {
            badRequest(result.cause());
        } else {
            if (result.result() == null) {
                success();
            } else {
                success(result.result());
            }
        }
    }

    protected <T> void handlerCreated(AsyncResult<T> result) {
        if (result.failed()) {
            badRequest(result.cause());
        } else {
            if (result.result() == null) {
                created();
            } else {
                created(result.result());
            }
        }
    }
}
