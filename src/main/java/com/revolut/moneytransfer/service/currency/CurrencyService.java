package com.revolut.moneytransfer.service.currency;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import org.jspare.core.annotation.Component;
import org.jspare.vertx.annotation.RegisterProxyService;

@ProxyGen
@Component
@RegisterProxyService(CurrencyService.SERVICE)
public interface CurrencyService {
    String SERVICE = "moneytransfer.service.currency";

    void getConvertedCount(double count,
                           String currencyFrom,
                           String currencyTo,
                           Handler<AsyncResult<Double>> resultHandler);

    void hasCurrency(String currencyCode, Handler<AsyncResult<Boolean>> resultHandler);
}
