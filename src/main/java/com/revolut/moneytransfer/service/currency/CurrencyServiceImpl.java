package com.revolut.moneytransfer.service.currency;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;

import java.util.Currency;

public class CurrencyServiceImpl implements CurrencyService {
    @Override
    public void getConvertedCount(double count,
                                  String currencyFrom,
                                  String currencyTo,
                                  Handler<AsyncResult<Double>> resultHandler) {
        if (hasCurrency(currencyFrom) && hasCurrency(currencyTo)) {
            // TODO: use real conversion data here
            double result = currencyFrom.equals(currencyTo) ? count : count * 2;
            resultHandler.handle(Future.succeededFuture(result));
        } else {
            resultHandler.handle(Future.failedFuture("No currency"));
        }
    }

    @Override
    public void hasCurrency(String currencyCode, Handler<AsyncResult<Boolean>> resultHandler) {
        resultHandler.handle(Future.succeededFuture(hasCurrency(currencyCode)));
    }

    private boolean hasCurrency(String currencyCode) {
        try {
            Currency.getInstance(currencyCode);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
