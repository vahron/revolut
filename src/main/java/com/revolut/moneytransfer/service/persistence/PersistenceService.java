package com.revolut.moneytransfer.service.persistence;

import com.github.susom.database.DbCode;
import com.github.susom.database.DbCodeTyped;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import org.jspare.core.annotation.Component;

@Component
public interface PersistenceService {
    void transact(DbCode code);

    <T> void transactAsync(DbCodeTyped<T> code, Handler<AsyncResult<T>> resultHandler);
}
