package com.revolut.moneytransfer.service.persistence;

import com.github.susom.database.*;
import com.revolut.moneytransfer.configuration.ConfigurationService;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import org.jspare.core.annotation.Inject;
import org.jspare.vertx.annotation.VertxInject;

public class PersistenceServiceImpl implements PersistenceService {
    private static final String DATABASE_URL_KEY = "persistence.database.url";

    @Inject
    private ConfigurationService config;
    @VertxInject
    private Vertx vertx;

    private DatabaseProvider.Pool pool;

    private DatabaseProvider.Pool getOrCreatePool() {
        if (pool == null) {
            Config databaseConfig =
                    Config.from().value("database.url", config.getConfig().getString(DATABASE_URL_KEY)).get();
            pool = DatabaseProvider.createPool(databaseConfig);
        }
        return pool;
    }

    @Override
    public void transact(DbCode code) {
        DatabaseProvider.fromPool(getOrCreatePool()).transact(code);
    }

    @Override
    public <T> void transactAsync(DbCodeTyped<T> code, Handler<AsyncResult<T>> asyncResultHandler) {
        DatabaseProviderVertx.fromPool(vertx, getOrCreatePool()).transactAsync(code, asyncResultHandler);
    }
}
