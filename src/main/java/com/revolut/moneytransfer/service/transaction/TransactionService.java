package com.revolut.moneytransfer.service.transaction;

import com.revolut.moneytransfer.model.transaction.Transaction;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import org.jspare.core.annotation.Component;
import org.jspare.vertx.annotation.RegisterProxyService;

import java.util.List;

@ProxyGen
@Component
@RegisterProxyService(TransactionService.SERVICE)
public interface TransactionService {
    String SERVICE = "moneytransfer.service.transaction";

    void createTransaction(long fromAccountId,
                           long toAccountId,
                           double count,
                           Handler<AsyncResult<Transaction>> resultHandler);

    void processTransaction(long id, Handler<AsyncResult<Transaction>> resultHandler);

    void getTransaction(long id, Handler<AsyncResult<Transaction>> resultHandler);

    void getAllTransactions(Handler<AsyncResult<List<Transaction>>> resultHandler);
}
