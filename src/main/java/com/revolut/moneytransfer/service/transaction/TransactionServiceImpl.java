package com.revolut.moneytransfer.service.transaction;

import com.revolut.moneytransfer.configuration.ConfigurationService;
import com.revolut.moneytransfer.model.transaction.Transaction;
import com.revolut.moneytransfer.model.transaction.TransactionDAO;
import com.revolut.moneytransfer.model.transaction.TransactionStatus;
import com.revolut.moneytransfer.model.user.User;
import com.revolut.moneytransfer.model.user.UserDAO;
import com.revolut.moneytransfer.service.currency.CurrencyService;
import com.revolut.moneytransfer.service.persistence.PersistenceService;
import com.revolut.moneytransfer.service.transaction.exception.InvoiceExpiredException;
import com.revolut.moneytransfer.service.transaction.exception.UserHasNoMoneyException;
import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import org.joda.time.DateTimeUtils;
import org.jspare.core.annotation.Inject;
import org.jspare.core.annotation.Resource;
import org.jspare.vertx.annotation.VertxProxyInject;

import java.util.List;

@Resource
public class TransactionServiceImpl implements TransactionService {
    private static final String INVOICE_TTL_KEY = "transaction.invoice.ttl.ms";

    @Inject
    private ConfigurationService config;
    @Inject
    private PersistenceService persistenceService;
    @Inject
    private TransactionDAO transactionDAO;
    @Inject
    private UserDAO userDAO;

    @VertxProxyInject(CurrencyService.SERVICE)
    private CurrencyService currencyService;

    @Override
    public void createTransaction(long fromAccountId,
                                  long toAccountId,
                                  double requestedCount,
                                  Handler<AsyncResult<Transaction>> resultHandler) {
        if (requestedCount <= 0) {
            resultHandler.handle(Future.failedFuture("Incorrect request count"));
            return;
        }
        if (fromAccountId == toAccountId) {
            resultHandler.handle(Future.failedFuture("Can't transfer to self"));
            return;
        }

        Future<User> fromUser = Future.future();
        Future<User> toUser = Future.future();
        persistenceService.transactAsync(dbs -> userDAO.getUser(dbs, fromAccountId), fromUser);
        persistenceService.transactAsync(dbs -> userDAO.getUser(dbs, toAccountId), toUser);

        CompositeFuture.all(fromUser, toUser).compose(fetchedUsersFuture -> {
            Future<Double> convertedCount = Future.future();
            currencyService.getConvertedCount(requestedCount, fromUser.result().getCurrency(),
                                              toUser.result().getCurrency(), convertedCount);
            return convertedCount;
        }).setHandler(event -> persistenceService.transactAsync(
                dbs -> transactionDAO.createTransaction(dbs,
                                                        fromAccountId,
                                                        toAccountId,
                                                        requestedCount,
                                                        event.result()),
                resultHandler
        ));
    }

    @Override
    public void processTransaction(long id, Handler<AsyncResult<Transaction>> resultHandler) {
        persistenceService.transactAsync(dbs -> {
            Transaction transaction = transactionDAO.getTransaction(dbs, id);
            if (transaction.getStatus() == TransactionStatus.DONE
                    || transaction.getStatus() == TransactionStatus.FAILED) {
                return transaction;
            }
            if (DateTimeUtils.currentTimeMillis() - transaction.getTimestamp()
                    > config.getConfig().getLong(INVOICE_TTL_KEY)) {
                throw new InvoiceExpiredException();
            }

            User from = userDAO.changeBalance(dbs, transaction.getFromAccountId(), -transaction.getRequestedCount());
            if (from.getBalance() < 0) {
                throw new UserHasNoMoneyException();
            }

            userDAO.changeBalance(dbs, transaction.getToAccountId(), transaction.getConvertedCount());

            return transactionDAO.changeTransactionStatus(dbs, id, TransactionStatus.DONE);
        }, resultHandler);
    }

    @Override
    public void getTransaction(long id, Handler<AsyncResult<Transaction>> resultHandler) {
        persistenceService.transactAsync(dbs -> transactionDAO.getTransaction(dbs, id), resultHandler);
    }

    @Override
    public void getAllTransactions(Handler<AsyncResult<List<Transaction>>> resultHandler) {
        persistenceService.transactAsync(dbs -> transactionDAO.getAllTransactions(dbs), resultHandler);
    }
}
