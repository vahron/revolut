package com.revolut.moneytransfer.service.user;

import com.revolut.moneytransfer.model.user.User;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import org.jspare.core.annotation.Component;
import org.jspare.vertx.annotation.RegisterProxyService;

import java.util.List;

@ProxyGen
@Component
@RegisterProxyService(UserService.SERVICE)
public interface UserService {
    String SERVICE = "moneytransfer.service.user";

    void createUser(String name,
                    String currencyCode,
                    Handler<AsyncResult<User>> resultHandler);

    void fetchAllUsers(Handler<AsyncResult<List<User>>> resultHandler);

    void getUser(long id, Handler<AsyncResult<User>> resultHandler);

    void changeBalance(Long id, Double change, Handler<AsyncResult<User>> resultHandler);
}
