package com.revolut.moneytransfer.service.user;

import com.revolut.moneytransfer.model.user.User;
import com.revolut.moneytransfer.model.user.UserDAO;
import com.revolut.moneytransfer.service.currency.CurrencyService;
import com.revolut.moneytransfer.service.persistence.PersistenceService;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import org.apache.commons.lang.StringUtils;
import org.jspare.core.annotation.Inject;
import org.jspare.vertx.annotation.VertxProxyInject;

import java.util.List;

public class UserServiceImpl implements UserService {
    @Inject
    private PersistenceService persistenceService;
    @Inject
    private UserDAO userDAO;
    @VertxProxyInject(CurrencyService.SERVICE)
    private CurrencyService currencyService;

    @Override
    public void createUser(String name, String currencyCode, Handler<AsyncResult<User>> resultHandler) {
        if (StringUtils.isBlank(name)) {
            resultHandler.handle(Future.failedFuture("Incorrect arguments"));
            return;
        }
        currencyService.hasCurrency(currencyCode, currencyExistsEvent -> {
            if (currencyExistsEvent.result()) {
                persistenceService.transactAsync(dbs -> userDAO.createUser(dbs, name, currencyCode), resultHandler);
            } else resultHandler.handle(Future.failedFuture("Wrong currency"));
        });
    }

    @Override
    public void fetchAllUsers(Handler<AsyncResult<List<User>>> resultHandler) {
        persistenceService.transactAsync(dbs -> userDAO.fetchAllUsers(dbs), resultHandler);
    }

    @Override
    public void getUser(long id, Handler<AsyncResult<User>> resultHandler) {
        persistenceService.transactAsync(dbs -> userDAO.getUser(dbs, id), resultHandler);
    }

    @Override
    public void changeBalance(Long id, Double change, Handler<AsyncResult<User>> resultHandler) {
        persistenceService.transactAsync(dbs -> userDAO.changeBalance(dbs, id, change), resultHandler);
    }
}
