package com.revolut.moneytransfer.base;

import com.github.susom.database.DbCode;
import com.revolut.moneytransfer.StartVerticle;
import com.revolut.moneytransfer.configuration.DefaultConfig;
import com.revolut.moneytransfer.model.transaction.Transaction;
import com.revolut.moneytransfer.model.transaction.TransactionDAO;
import com.revolut.moneytransfer.model.transaction.TransactionStatus;
import com.revolut.moneytransfer.model.user.User;
import com.revolut.moneytransfer.model.user.UserDAO;
import com.revolut.moneytransfer.service.persistence.PersistenceService;
import io.vertx.core.*;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.RunTestOnContext;
import io.vertx.ext.unit.junit.Timeout;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.jspare.core.container.Environment;
import org.jspare.vertx.bootstrap.EnvironmentUtils;
import org.jspare.vertx.bootstrap.VerticleInitializer;
import org.jspare.vertx.bootstrap.VertxJspareLauncher;
import org.jspare.vertx.utils.ReflectionUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.util.LinkedList;
import java.util.List;

import static org.jspare.core.container.Environment.my;

@RunWith(VertxUnitRunner.class)
public abstract class VertxTestBase {
    @Rule
    public RunTestOnContext rule = new RunTestOnContext();

    @Rule
    public Timeout timeoutRule = Timeout.seconds(60);

    private int serverPort;

    @Before
    public void setup(TestContext testContext) {
        Class<?> testClass = this.getClass();
        Object thisTest = this;
        Vertx vertx = rule.vertx();

        serverPort = findFreePort();
        DefaultConfig.putDefaultConfig("http.port", serverPort);
        DefaultConfig.putDefaultConfig("persistence.database.url",
                                       String.format("jdbc:hsqldb:mem:%s;shutdown=true", testClass.getSimpleName()));

        Async serverAsync = testContext.async();

        prepareTestContext(vertx);

        // when server started
        vertx.eventBus().consumer(StartVerticle.STARTED_CHANNEL).handler(startedEvent -> {
            vertx.executeBlocking(future -> {
                try {
                    runBeforeTest(testContext, testClass, thisTest, vertx);

                    List<Future> futures = runTestMethods(testContext, testClass, thisTest, vertx);
                    CompositeFuture.all(futures).setHandler(event -> {
                        if (event.succeeded()) future.complete();
                        else future.fail(event.cause());
                    });
                } catch (Exception e) {
                    future.fail(e);
                }
            }, (Handler<AsyncResult<Void>>) event -> {
                if (event.failed()) {
                    // hack to get an assertion
                    testContext.assertEquals("Test execution failed", event.cause());
                }
                rule.vertx().close();
                serverAsync.complete();
            });
        });
    }

    private void prepareTestContext(Vertx vertx) {
        vertx.runOnContext(event -> {
            Environment.release();

            VertxJspareLauncher launcher = new VertxJspareLauncher();
            vertx.getOrCreateContext().put(EnvironmentUtils.VERTX_HOLDER, rule.vertx());
            launcher.beforeStartingVertx(new VertxOptions());
            launcher.afterStartingVertx(rule.vertx());

            vertx.deployVerticle(VerticleInitializer.initialize(StartVerticle.class));
        });
    }

    private void runBeforeTest(TestContext testContext, Class<?> testClass, Object thisTest, Vertx vertx)
            throws IllegalAccessException, InvocationTargetException {
        List<Method> beforeMethods = ReflectionUtils
                .getMethodsWithAnnotation(testClass, VertxBeforeTestClass.class);
        assert beforeMethods.size() < 2;

        beforeMethods.get(0).invoke(thisTest, testContext, vertx);
    }

    private List<Future> runTestMethods(TestContext testContext, Class<?> testClass, Object thisTest, Vertx vertx)
            throws IllegalAccessException, InvocationTargetException {
        List<Method> testMethods = ReflectionUtils
                .getMethodsWithAnnotation(testClass, VertxTest.class);

        List<Future> result = new LinkedList<>();
        for (Method method : testMethods) {
            Future future = Future.future();
            result.add(future);
            method.invoke(thisTest, testContext, vertx, future);
        }
        return result;
    }

    // empty test for Junit Test Runner
    @Test
    public void testSuite(TestContext context) {
    }

    protected HttpClient createHttpClient() {
        HttpClientOptions clientOptions = new HttpClientOptions();
        clientOptions.setConnectTimeout(30000);
        clientOptions.setIdleTimeout(30000);
        clientOptions.setDefaultHost("localhost");
        clientOptions.setDefaultPort(serverPort);
        return rule.vertx().createHttpClient(clientOptions);
    }

    private static int findFreePort() {
        try {
            ServerSocket socket = new ServerSocket(0);
            int result = socket.getLocalPort();
            socket.close();
            return result;
        } catch (Exception e) {
            return 8080;
        }
    }

    protected User createUser() {
        return createUsers(1).get(0);
    }

    protected List<User> createUsers(int count) {
        List<User> result = new LinkedList<>();
        executeTransaction(dbs -> {
            for (int i = 0; i < count; i++) {
                UserDAO userDAO = my(UserDAO.class);
                User user = userDAO.createUser(dbs, "test-user" + i, "USD");
                result.add(userDAO.changeBalance(dbs, user.getId(), 200));
            }
        });
        return result;
    }

    protected User fetchUser(long id) {
        List<User> result = new LinkedList<>();
        executeTransaction(dbs -> result.add(my(UserDAO.class).getUser(dbs, id)));
        return result.get(0);
    }

    protected Transaction createTransaction(long fromAccountId,
                                            long toAccountId,
                                            double requestedCount,
                                            double convertedCount,
                                            TransactionStatus status) {
        List<Transaction> result = new LinkedList<>();
        executeTransaction(dbs -> {
            TransactionDAO transactionDAO = my(TransactionDAO.class);
            Transaction transaction = transactionDAO.createTransaction(dbs,
                                                                       fromAccountId,
                                                                       toAccountId,
                                                                       requestedCount,
                                                                       convertedCount);
            transaction = transactionDAO.changeTransactionStatus(dbs, transaction.getId(), status);
            result.add(transaction);
        });
        return result.get(0);
    }

    private void executeTransaction(DbCode transaction) {
        my(PersistenceService.class).transact(transaction);
    }
}