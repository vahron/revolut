package com.revolut.moneytransfer.restapi;

import com.revolut.moneytransfer.base.VertxBeforeTestClass;
import com.revolut.moneytransfer.base.VertxTest;
import com.revolut.moneytransfer.base.VertxTestBase;
import com.revolut.moneytransfer.model.transaction.Transaction;
import com.revolut.moneytransfer.model.transaction.TransactionStatus;
import com.revolut.moneytransfer.model.user.User;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.unit.TestContext;
import org.joda.time.DateTimeUtils;

import java.util.List;

public class TransactionRestServiceTest extends VertxTestBase {
    private List<User> users;

    @VertxBeforeTestClass
    public void beforeClass(TestContext context, Vertx vertx) {
        users = createUsers(10);
        DateTimeUtils.setCurrentMillisFixed(0);
    }

    @VertxTest
    public void fetchAllTransactions_succeed(TestContext context, Vertx vertx, Future testResult) {
        Transaction created =
                createTransaction(1, 2, 1D, 1D, TransactionStatus.INVOICE);

        createHttpClient().getNow("/v1/transaction", response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.OK.code());
            response.bodyHandler(body -> {
                JsonArray transactions = body.toJsonArray();
                context.assertTrue(!transactions.isEmpty());

                for (int i = 0; i < transactions.size(); i++) {
                    Transaction current = new Transaction(transactions.getJsonObject(i));
                    if (current.getId() == created.getId()) {
                        context.assertEquals(1L, current.getFromAccountId());
                        context.assertEquals(2L, current.getToAccountId());
                        context.assertEquals(1D, current.getRequestedCount());
                        context.assertEquals(1D, current.getConvertedCount());
                        context.assertEquals(0L, current.getTimestamp());
                    }
                }

                testResult.complete();
            });
        });
    }

    @VertxTest
    public void getTransactionById_succeed(TestContext context, Vertx vertx, Future testResult) {
        Transaction created = createTransaction(1, 2, 1D, 1D, TransactionStatus.INVOICE);
        createHttpClient().getNow("/v1/transaction/" + created.getId(), response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.OK.code());
            response.bodyHandler(body -> {
                context.assertEquals(created.toJson(), body.toJsonObject());
                testResult.complete();
            });
        });
    }

    @VertxTest
    public void getTransactionById_wrongId(TestContext context, Vertx vertx, Future testResult) {
        createHttpClient().getNow("/v1/transaction/123231", response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.BAD_REQUEST.code());
            testResult.complete();
        });
    }

    @VertxTest
    public void createNewUser_withoutParameters(TestContext context, Vertx vertx, Future testResult) {
        createHttpClient().post("/v1/transaction/", response -> {
            // jspare has internal error when trying to process omitted numeric parameters
            context.assertEquals(response.statusCode(), HttpResponseStatus.INTERNAL_SERVER_ERROR.code());
            testResult.complete();
        }).end();
    }

    @VertxTest
    public void createNewTransaction_incorrectParameters(TestContext context, Vertx vertx, Future testResult) {
        createHttpClient().post("/v1/transaction?fromAccountId=1&toAccountId=2&requestedCount=-2", response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.BAD_REQUEST.code());
            testResult.complete();
        }).end();
    }

    @VertxTest
    public void createNewTransaction_noUser(TestContext context, Vertx vertx, Future testResult) {
        createHttpClient().post("/v1/transaction?fromAccountId=1222&toAccountId=2&requestedCount=2", response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.BAD_REQUEST.code());
            testResult.complete();
        }).end();
    }

    @VertxTest
    public void createNewTransaction_succeed(TestContext context, Vertx vertx, Future testResult) {
        createHttpClient().post("/v1/transaction?fromAccountId=1&toAccountId=2&requestedCount=4", response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.CREATED.code());
            response.bodyHandler(body -> {
                Transaction created = new Transaction(body.toJsonObject());
                context.assertEquals(1L, created.getFromAccountId());
                context.assertEquals(2L, created.getToAccountId());
                context.assertEquals(4D, created.getRequestedCount());
                context.assertEquals(TransactionStatus.INVOICE, created.getStatus());
                context.assertEquals(0L, created.getTimestamp());
                testResult.complete();
            });
        }).end();
    }

    @VertxTest
    public void processTransaction_noTransaction(TestContext context, Vertx vertx, Future testResult) {
        createHttpClient().put("/v1/transaction/processByInvoice/23123", response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.BAD_REQUEST.code());
            testResult.complete();
        }).end();
    }

    @VertxTest
    public void processTransaction_alreadyDone(TestContext context, Vertx vertx, Future testResult) {
        Transaction transaction =
                createTransaction(1, 2, 2D, 4D, TransactionStatus.DONE);
        createHttpClient().put("/v1/transaction/processByInvoice/" + transaction.getId(), response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.OK.code());
            response.bodyHandler(body -> {
                context.assertEquals(transaction.toJson(), body.toJsonObject());
                testResult.complete();
            });
        }).end();
    }

    @VertxTest
    public void processTransaction_userHasNoMoney(TestContext context, Vertx vertx, Future testResult) {
        User fromUser = createUser();
        User toUser = createUser();

        Transaction transaction =
                createTransaction(fromUser.getId(), toUser.getId(), 400D, 4D, TransactionStatus.INVOICE);
        createHttpClient().put("/v1/transaction/processByInvoice/" + transaction.getId(), response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.BAD_REQUEST.code());
            testResult.complete();
        }).end();
    }

    @VertxTest
    public void processTransaction_succeed(TestContext context, Vertx vertx, Future testResult) {
        User fromUser = createUser();
        User toUser = createUser();

        Transaction transaction =
                createTransaction(fromUser.getId(), toUser.getId(), 2D, 4D, TransactionStatus.INVOICE);
        createHttpClient().put("/v1/transaction/processByInvoice/" + transaction.getId(), response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.OK.code());
            response.bodyHandler(body -> {
                Transaction complete = new Transaction(body.toJsonObject());

                context.assertEquals(TransactionStatus.DONE, complete.getStatus());
                context.assertEquals(0L, complete.getTimestamp());
                context.assertEquals(2D, fromUser.getBalance() - fetchUser(fromUser.getId()).getBalance());
                context.assertEquals(4D, fetchUser(toUser.getId()).getBalance() - toUser.getBalance());

                testResult.complete();
            });
        }).end();
    }
}