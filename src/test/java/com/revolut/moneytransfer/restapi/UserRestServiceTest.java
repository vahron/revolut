package com.revolut.moneytransfer.restapi;

import com.revolut.moneytransfer.base.VertxBeforeTestClass;
import com.revolut.moneytransfer.base.VertxTest;
import com.revolut.moneytransfer.base.VertxTestBase;
import com.revolut.moneytransfer.model.user.User;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.unit.TestContext;

public class UserRestServiceTest extends VertxTestBase {
    @VertxBeforeTestClass
    public void beforeClass(TestContext context, Vertx vertx) {
        createUsers(10);
    }

    @VertxTest
    public void fetchAllUsers_succeed(TestContext context, Vertx vertx, Future testResult) {
        createHttpClient().getNow("/v1/user", response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.OK.code());
            response.bodyHandler(event -> {
                JsonArray entries = event.toJsonArray();
                context.assertTrue(entries.size() > 3);

                User user = new User(entries.getJsonObject(0));
                context.assertEquals(user.getBalance(), 200.0);
                context.assertEquals(user.getCurrency(), "USD");
                context.assertTrue(user.getName().startsWith("test-user"));

                testResult.complete();
            });
        });
    }

    @VertxTest
    public void createNewUser_withoutParameters(TestContext context, Vertx vertx, Future testResult) {
        createHttpClient().post("/v1/user", response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.BAD_REQUEST.code());
            testResult.complete();
        }).end();
    }

    @VertxTest
    public void createNewUser_wrongCurrency(TestContext context, Vertx vertx, Future testResult) {
        createHttpClient().post("/v1/user?name=testName&currencyCode=ART", response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.BAD_REQUEST.code());
            testResult.complete();
        }).end();
    }

    @VertxTest
    public void createNewUser_succeed(TestContext context, Vertx vertx, Future testResult) {
        createHttpClient().post("/v1/user?name=testName&currencyCode=RUB", response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.CREATED.code());
            response.bodyHandler(body -> {
                User created = new User(body.toJsonObject());
                context.assertEquals(created.getName(), "testName");
                context.assertEquals(created.getCurrency(), "RUB");
                context.assertEquals(created.getBalance(), 0D);
                testResult.complete();
            });
        }).end();
    }

    @VertxTest
    public void getUserById_wrongId(TestContext context, Vertx vertx, Future testResult) {
        createHttpClient().getNow("/v1/user/12321", response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.BAD_REQUEST.code());
            testResult.complete();
        });
    }

    @VertxTest
    public void getUserById_succeed(TestContext context, Vertx vertx, Future testResult) {
        User created = createUser();
        createHttpClient().getNow("/v1/user/" + created.getId(), response -> {
            context.assertEquals(response.statusCode(), HttpResponseStatus.OK.code());
            response.bodyHandler(body -> {
                context.assertEquals(created.toJson(), body.toJsonObject());
                testResult.complete();
            });
        });
    }
}